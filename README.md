# console-to-web-app

DotNet sample on how to convert a console project into a web app

## Steps
1. Modify from `<Project Sdk="Microsoft.NET.Sdk">` to `<Project Sdk="Microsoft.NET.Sdk.Web">`
2. Remove line `<OutputType>Exe</OutputType>`

## What is a host
It's an object that encapsulates the resources of an application 
(e.g: DI, logging, configuration, `IHostedService` implementation)

## Links
https://www.youtube.com/watch?v=9QQSPFtZ6Uk